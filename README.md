# justJava
Just Java app, will allow a user to fill out a form to order coffee.


### Objectives
Introduction to basic fundamentals of Java such as Intents.

### Build Instructions
This sample uses the Gradle build system. To build this project, use the
"gradlew build" command or use "Import Project" in Android Studio.

### screenshots
![please find images under app-screenshots directory](app-screenshots/screen-1.png "screen one")
![please find images under app-screenshots directory](app-screenshots/screen-2.png "screen two")
![please find images under app-screenshots directory](app-screenshots/screen-3.png "screen three")
